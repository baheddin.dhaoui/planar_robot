#include "control.h"
#include "trajectory_generation.h"
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
int count=0;
  std::ofstream myfile;
  myfile.open(" 2DOF.csv");
  // // myfile << "This is the first cell in the first column.\n";
  myfile << "q0,q1,xdes,ydes,xactual,yactual\n";

  const double DtIn=2;
  const double dt=1e-1;
  double l1In=0.1;
  double l2In=0.1;
  float t=0;

  Eigen::Vector2d X_f (0.0 , 0.6);
  Eigen::Vector2d xd,Xa,X_i,Dxd_ff,Dqd;
  Eigen::Matrix2d J;
 

  RobotModel model1(l1In,l2In);
  Controller controller1(model1);

  Eigen::Vector2d q(M_PI_2, M_PI_4);
  model1.FwdKin(X_i,J,q);

  Point2Point track1(X_i,X_f,DtIn);

  while (t<DtIn){
    
    xd= track1.X(t);
    Dxd_ff= track1.dX(t);
    
    Dqd= controller1.Dqd(q,xd,Dxd_ff);
    
    q= (Dqd*dt) + q;
    
    model1.FwdKin(Xa,J,q);
    if (count%290){

    myfile <<q[0]<<","\
      <<q[1]<<","\
      <<xd[0]<<","\
      <<xd[1]<<","\
      <<Xa[1]<<","\
      <<Xa[1]<<endl;

    };
    count=count+1;
    t =t+dt;
  }
  
}
