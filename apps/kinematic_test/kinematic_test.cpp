#include "kinematic_model.h"
#include <iostream>
using namespace std;
int main(){
  // Compute the forward kinematics and jacobian matrix for 
RobotModel f (1,1);
Eigen::Vector2d xOut0,xOut1,dq;
Eigen::Matrix2d JOut0,JOut1;
Eigen::Vector2d qIn={M_PI/3.0,M_PI_4};



f.FwdKin(xOut0,JOut0,qIn);
cout<<"x and y \n"<<xOut0<<endl;
cout<<"Jacobian \n"<<JOut0<<endl;

  // For a small variation of q, compute the variation on X and check dx = J . dq  
dq={0.001,0.001};
qIn=qIn+dq;
cout<<qIn<<endl;
// qIn(1)=qIn(1)+0.01;
f.FwdKin(xOut1,JOut1,qIn);

cout<<"x and y \n"<<xOut1<<endl;
cout<<"Jacobian \n"<<JOut1<<endl;

cout<<"variation numirically \n"<<xOut1-xOut0<<endl;
cout<<"variation analytically \n"<<JOut0*dq<<endl;



  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq
}