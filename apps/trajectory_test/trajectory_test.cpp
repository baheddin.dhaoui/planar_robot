#include "trajectory_generation.h"
#include <iostream>

using namespace std;

int main()

{ 
  Eigen::Vector2d Init_position = {2,10};
  Eigen::Vector2d Final_position ={10,15};

  double Dt=10  ;

Point2Point Pos(Init_position,Final_position,Dt);

double t_i=0;

float delta_t=0.02;
  for(double time=t_i;time<t_i+Dt;time+=delta_t){

    cout<<"\n Velocity="<<Pos.dX(time)[0];//velocities
    cout<<"\n Numerically ="<< ((Pos.X(time+delta_t)[0])-(Pos.X(time)[0]))/delta_t;

  //Positions
    cout << "Pos\t"<< (Pos.dX(time)[0]-((Pos.X(time+delta_t)[0])-(Pos.X(time)[0]))/delta_t);
}
}
  // Compute the trajectory for given initial and final positions.
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities