#include "control.h"
#include <iostream>
#include <Eigen/Dense>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{
}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
  Eigen::Vector2d Dqd;
  Eigen::Vector2d x;

  model.FwdKin(x,J,q);
  dX_desired=kp*(xd-X);

  Dqd=J.inverse()*(dX_desired+Dxd_ff);

  return Dqd;
  
  
  
  
  
}

