#include "Kinematic_model.h"
#include <iostream>

RobotModel::RobotModel(const double &l1In, const double &l2In){

}

RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d & qIn){
  // TODO Implement the forward and differential kinematics

//Eigen::Vector3d reference_point_position(0.0,0.0,0.0);
//Eigen::MatrixXd jacobian;
  double x = l1*cos(qIn[0])+l2*cos(qIn[0]+qIn[1]);
  double y = l1*sin(qIn[0])+l2*sin(qIn[0]+qIn[1]);
  xOut={x,y};

// thetas x
  double dx_theta1=-l1*sin(qIn[0])-l2*sin(qIn[0]+qIn[1]);
  double dx_theta2=-l2*sin(qIn[0]+qIn[1]);

// thetas y
  double dy_theta1=l2*cos(qIn[0]+qIn[1])+l1*cos(qIn[0]);
  double dy_theta2=l2*cos(qIn[0]+qIn[1]);

  Eigen::Matrix2d f;
  f<<dx_theta1,dx_theta2,dy_theta1,dy_theta2;
  
  JOut=f;

}