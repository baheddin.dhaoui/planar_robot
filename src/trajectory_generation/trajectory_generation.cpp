#include "trajectory_generation.h"
#include <iostream>



Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
 a={piIn,0,0,10,-15,6};
 pi=piIn;
 pf=piIn;
 Dt=DtIn;
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
pi=piIn;
pf=pfIn;
Dt=DtIn;

};

const double  Polynomial::p   (const double &t){
  //TODO compute position

  
  double p=a[0]+a[1]*(pf-pi)*pow((t/Dt),1)+a[2]*(pf-pi)*pow((t/Dt),2)+a[3]*(pf-pi)*pow((t/Dt),3)+a[4]*pow((t/Dt),4)+a[5]*(pf-pi)*pow((t/Dt),5);
  
  return p;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity

 double dp = a[1]/Dt + 2*a[2]*t/pow(Dt,2) + 3*a[3]*pow(t,2)/pow(Dt,3) + 4*a[4]*pow(t,3)/pow(Dt,4) + 5*a[5]*pow(t,4)/pow(Dt,5);
 //double p1= 3*X[3]*(Pf-Pi)*pow(t,2)+4*X[4]*(Pf-Pi)*pow(t,3)+5*X[5]*(Pf-Pi)*pow(t,4);
return dp;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //TODO initialize object and polynomials


  polx = Polynomial(xi(0),xf(0),DtIn); // Polynomial to x coordinates
  poly = Polynomial (xi(1),xf(1),DtIn); // Polynomial to y coordinates
  Dt=DtIn; 



}

Eigen::Vector2d Point2Point::X(const double & time){
  //TODO compute cartesian position
  Dt=time;
  Eigen::Vector2d carposition;

  carposition={polx.p(Dt),poly.p(Dt)};

  //return Eigen::Vector2d::Zero(carposition);
return carposition;
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Dt=time;
  Eigen::Vector2d carvelocity;

  carvelocity={polx.dp(Dt),poly.dp(Dt)};
  
  return carvelocity;
}